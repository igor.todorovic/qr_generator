<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use chillerlan\QRCode\QRCode;
use chillerlan\QRCode\QROptions;


class QrCodeGeneratorController extends AbstractController
{
    #[Route('/', name: 'app_qr_codes')]
    public function index(Request $request)
    {
        $url = "";
        $form = $this->createFormBuilder()
            ->add('url', UrlType::class, [
                'label' => 'Enter a URL:',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'https://example.com'
                ]
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Generate QR Code',
                'attr' => [
                    'class' => 'btn btn-primary'
                ]
            ])
            ->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $task = $form->getData();

            $url = $task["url"];

            $full_url = 'https://chart.googleapis.com/chart?chs=500x500&cht=qr&chl='.$url.'&choe=UTF-8';
            $img_content = file_get_contents($full_url);

            header("Cache-Control: public");
            header("Content-Description: File Transfer");
            header("Content-Disposition: attachment; filename=qr.png");
            header("Content-Type: image/png");
            header("Content-Transfer-Encoding: binary");
            echo $img_content;
        }

        return $this->render('qr_code/index.html.twig', [
            'form' => $form->createView(),
            'url' => $url
        ]);
    }

}
